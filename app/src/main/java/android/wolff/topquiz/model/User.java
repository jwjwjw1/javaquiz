package android.wolff.topquiz.model;

import androidx.annotation.NonNull;

/**
 * Wolff Jérémy - 04.05.2020
 **/
public class User {
    // Préfixe "m" pour "member": il s'agit d'un attribut de classe
    private String mFirstname;

    public String getFirstname() {
        return mFirstname;
    }

    public void setFirstname(String firstname) {
        mFirstname = firstname;
    }

    @Override
    public String toString() {
        return "User{" +
                "mFirstname='" + mFirstname + '\'' +
                '}';
    }
}
