package android.wolff.topquiz.controller;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.*;
import android.wolff.topquiz.R;
import android.wolff.topquiz.model.Question;
import android.wolff.topquiz.model.QuestionBank;

import java.util.Arrays;

public class GameActivity extends AppCompatActivity implements View.OnClickListener{

    // Attributs
    private TextView mQuestionTextView;
    private Button mAnswerButton1;
    private Button mAnswerButton2;
    private Button mAnswerButton3;
    private Button mAnswerButton4;

    private QuestionBank mQuestionBank;
    private Question mCurrentQuestion;

    private int mScore;
    private int mNumberOfQuestions;

    public static final String BUNDLE_EXTRA_SCORE = "BUNDLE_EXTRA_SCORE";
    public static final String BUNDLE_STATE_SCORE = "currentScore";
    public static final String BUNDLE_STATE_QUESTION = "currentQuestion";

    private boolean mEnableTouchEvents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        System.out.println("GameActivity::onCreate()");

        mQuestionBank = this.generateQuestions();

        if (savedInstanceState != null) {
            mScore = savedInstanceState.getInt(BUNDLE_STATE_SCORE);
            mNumberOfQuestions = savedInstanceState.getInt(BUNDLE_STATE_QUESTION);
        } else {
            mScore = 0;
            mNumberOfQuestions = 10;
        }

        mEnableTouchEvents = true;

        // Wire widgets
        mQuestionTextView = (TextView) findViewById(R.id.activity_game_question_text);
        mAnswerButton1 = (Button) findViewById(R.id.activity_game_answer1_btn);
        mAnswerButton2 = (Button) findViewById(R.id.activity_game_answer2_btn);
        mAnswerButton3 = (Button) findViewById(R.id.activity_game_answer3_btn);
        mAnswerButton4 = (Button) findViewById(R.id.activity_game_answer4_btn);

        // Use the tag property to 'name' the buttons
        mAnswerButton1.setTag(0);
        mAnswerButton2.setTag(1);
        mAnswerButton3.setTag(2);
        mAnswerButton4.setTag(3);

        mAnswerButton1.setOnClickListener(this);
        mAnswerButton2.setOnClickListener(this);
        mAnswerButton3.setOnClickListener(this);
        mAnswerButton4.setOnClickListener(this);

        mCurrentQuestion = mQuestionBank.getQuestion();
        this.displayQuestion(mCurrentQuestion);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(BUNDLE_STATE_SCORE, mScore);
        outState.putInt(BUNDLE_STATE_QUESTION, mNumberOfQuestions);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View v) {
        int responseIndex = (int) v.getTag();

        if (responseIndex == mCurrentQuestion.getAnswerIndex()) {
            // Good answer
            Toast.makeText(this, "C'est juste!", Toast.LENGTH_SHORT).show();
            mScore++;
        } else {
            // Wrong answer
            Toast.makeText(this, "Dommage!", Toast.LENGTH_SHORT).show();
        }

        mEnableTouchEvents = false;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mEnableTouchEvents = true;

                // If this is the last question, ends the game.
                // Else, display the next question.
                if (--mNumberOfQuestions == 0) {
                    // End the game
                    endGame();
                } else {
                    mCurrentQuestion = mQuestionBank.getQuestion();
                    displayQuestion(mCurrentQuestion);
                }
            }
        }, 2000); // LENGTH_SHORT is usually 2 seconds long
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return mEnableTouchEvents && super.dispatchTouchEvent(ev);
    }

    private void endGame() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Le quiz est terminé!")
                .setMessage("Voici ton score: " + mScore)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // End the activity
                        Intent intent = new Intent();
                        intent.putExtra(BUNDLE_EXTRA_SCORE, mScore);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }

    private void displayQuestion(final Question question) {
        // Set the text for the question text view and the four buttons
        mQuestionTextView.setText(question.getQuestion());
        mAnswerButton1.setText(question.getChoiceList().get(0));
        mAnswerButton2.setText(question.getChoiceList().get(1));
        mAnswerButton3.setText(question.getChoiceList().get(2));
        mAnswerButton4.setText(question.getChoiceList().get(3));

    }

    private QuestionBank generateQuestions() {
        Question question1 = new Question("Qui a inventé Java?",
                Arrays.asList("James Gosling et Patrick Naughton ", "Steve Jobs et Bill Gates", "R. Rivest, A. Shamir et L. Adleman", "Linus Torvalds et Tim Berners-Lee"),
                0);

        Question question2 = new Question("Java a été inventé par deux ingénieurs en 1991 chez...",
                Arrays.asList("IBM", "Intel Corporation", "SUN MicroSystems", "FaceBook"),
                2);

        Question question3 = new Question("Qui est le créateur du système d'exploitation Android?",
                Arrays.asList("Andy Rubin", "Steve Wozniak", "Jake Wharton", "Paul Smith"),
                0);

        Question question4 = new Question("L'environnement nécessaire à la compilation du code JAVA dans le but de générer du code intermédiaire est...",
                Arrays.asList("le JRE (Java Runtime Environment", "le JDK (Java Development Kit)", "l'API (Interface de Programmation d'Application)", "la clé asymétrique RSA"),
                1);

        Question question5 = new Question("Quelles sont les conditions nécessaires pour qu'un client puisse exécuter le fichier suivant : monprogramme.jar ?",
                Arrays.asList("Disposer d'une RAM de 16 Go minimum", "Etre connecté au réseau WAN", "Posséder une clé de déchiffrement", "Disposer du JRE propre à l'OS du client"),
                3);

        Question question6 = new Question("Que signifie OOP (anglais) en programmation?",
                Arrays.asList("Personal Ordered Object", "Pipeline Of Overvoice", "Object-Oriented Programmation", "Private Object Ordered"),
                2);

        Question question7 = new Question("Comment appelle-t-on le standard de représentation graphique en POO?",
                Arrays.asList("UML", "SDK", "JRE", "RSA"),
                0);

        Question question8 = new Question("Trois piliers fondamentaux de la POO...",
                Arrays.asList("Intégration, polyvalence et structuration", "Encapsulation, héritage, polymorphisme", "Décapsulation, héritage et polythéisme", "Protection, service et autorité"),
                1);

        Question question9 = new Question("En POO, les éléments qui caractérisent une classe sont...",
                Arrays.asList("les attributs et les méthodes", "les enseignants et les élèves", "les attributs et les rôles", "les chaînes de caractères et le JDK"),
                0);

        Question question10 = new Question("Quels sont les éléments minimums à déclarer et à définir dans une classe? ",
            Arrays.asList("La disposition spaciale du matériel", "Le respect d'autrui et la discipline", "Le rôle de chef et du subordonné", "Les attributs, constructeur par défaut et méthodes"),
            3);

        Question question11 = new Question("Que se passe-t-il lorsque le mot réservé \"new\" est exécuté dans un EDI?",
                Arrays.asList("Un fichier .xml s'ouvre", "L'instanciation de la classe en mémoire est déclenchée", "Une méthode est créée", "Une fenêtre du navigateur s'ouvre"),
                1);

        Question question12 = new Question("A quoi sert le mot réservé \"extends\"?",
                Arrays.asList("A étendre l'écran automatiquement", "A créer un hyperlien", "A définir un héritage de classe (dérivation)", "A implémenter une interface"),
                2);

        Question question13 = new Question("De quoi parle-t-on quand on utilise le mot \"instance\" en POO?",
                Arrays.asList("De la copie conforme d'une classe, matérialisée par l'objet en mémoire", "De l'organisme qui exerce le pouvoir de décision et d'autorité", "De la compilation d'un programme", "De la mise en ligne du produit"),
                0);

        Question question14 = new Question("En bref, qu'est-ce que la compilation d'un programme?",
                Arrays.asList("L'implémentaton d'une interface graphique", "Le processus de transformation du code source en code objet", "Un enregistrement de chansons à succès", "Un rapport d'analyse qui indique le taux de plagiat d'un fichier"),
                1);

        Question question15 = new Question("Les portées possibles (protections) applicables aux attributs et méthodes d'une classe sont les suivantes:",
                Arrays.asList("table, form et div", "String, cullote et shorty", "int, float et String", "private, protected, public"),
                3);

        Question question16 = new Question("Comment appelle-t-on les méthodes d'une classe qui accèdent aux attributs protégés ? ",
                Arrays.asList("Les mutants (setters)", "Les mutateurs (setters)", "Les méthodes expéditives", "Les réglages système (setters)"),
                1);

        Question question17 = new Question("Une classe fille est une sous-classe, une classe mère est une...",
                Arrays.asList("super classe", "sur classe", "over classe", "classe semi-fermée"),
                0);

        Question question18 = new Question("L'héritage multiple est-il possible en Java?",
                Arrays.asList("Non", "Oui", "Je ne sais pas et je perds un point bêtement...", "J'ai faim..."),
                0);

        Question question19 = new Question("Que signifie le terme \"package\" dans un EDI? ",
                Arrays.asList("Que le mot de passe a été haché avec un algorithme SHA.", "Qu'il y a un problème lors de l'exécution du programme.", "C'est le projet contenant toutes les ressources nécessaires à la compilation du byte code.", "C'est une charte à accepter pour compiler le code source."),
                2);

        Question question20 = new Question("La redéfinition, qui consiste à ajouter des fonctionnalités à une méthode héritée, s'annote:",
                Arrays.asList("@Overdose", "@Overload", "@Deprecated", "@Override"),
                3);

        Question question21 = new Question("L'instanciation correcte d'une classe \"Fenetre\" est la suivante:",
                Arrays.asList("Fenetre maFenetre = new Fenetre();", "Fen fen = new Fen()", "mafenetre Fenetre = new Fenetre();", "Aucune n'est correcte..."),
                0);

        Question question22 = new Question("Comment (syntaxiquement) appelle-t-on le constructeur par défaut de la classe mère lors d'un héritage?",
                Arrays.asList("Avec le mot réservé switch", "Avec le mot réservé abstract", "Avec le mot réservé super", "Avec le mot réservé final"),
                2);

        Question question23 = new Question("Peut-on dériver une classe déclarée \"final\" ?",
                Arrays.asList("Oui", "Non", "J'ai soif...", "Je ne sais pas et je perds un point bêtement..."),
                1);


        return new QuestionBank(Arrays.asList(question1,
                question2,
                question3,
                question4,
                question6,
                question7,
                question8,
                question9,
                question10,
                question11,
                question12,
                question13,
                question14,
                question15,
                question16,
                question17,
                question18,
                question19,
                question20,
                question21,
                question22,
                question23));
    }

    @Override
    protected void onStart() {
        super.onStart();

        System.out.println("GameActivity::onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();

        System.out.println("GameActivity::onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();

        System.out.println("GameActivity::onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();

        System.out.println("GameActivity::onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        System.out.println("GameActivity::onDestroy()");
    }
}
